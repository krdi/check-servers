#!/usr/bin/env python2

import argparse
import os
import subprocess
import shutil
import time
import re
import sys

class SSHTool(object):

	def __init__(self, args):
		self.agent_IP = ''
		self.IPs = []
		self.IPs_pids = {}

		self.ssh_version_cmd = "echo -e \"QUIT\n\" | nc  -G 5".split(" ")
		self.ssh_version_cmd = "nc -i 1"
		self.staging_dir = ''
		self.working_dir = ""
		self.version = ""
		self.master_pid = ''
		self.agent_pids = []
		self.agent_pid = 0
		if args.pri_dir is None:
			self.pri_dir = '~/.ssh/'
		else:
			self.ssh_user = args.ssh_dir
		if args.pri is None:
			self.pri = 'developer_key'
		else:
			self.pri = args.pri
		if args.ssh_user is None:
			self.ssh_user = 'developer'
		else:
			self.ssh_user = args.ssh_user
		self.ssh_agent_cmd = 'ssh -o StrictHostKeyChecking=no -o PasswordAuthentication=no -i ' + self.pri_dir +self.pri
		self.sshd_update_centos = 'sudo yum -y update openssh-server'
		self.sshd_update_ubuntu = '"sudo apt -y update && sudo apt -y install openssh-server"'

		self.mlogfile = open('master' + '.log', "a", 1)
		self.errors = 0
		self.release = ''

	def set_agent_IP(self, IP):
		self.agent_IP = IP
		self.IPs.append(IP)

	def get_IPs(self):
		return self.IPs

	def get_all_pids(self):
		return self.agent_pids

	def fork_agent(self):
		pid = os.fork()
		if pid == 0:
			childs_pid = os.getpid()
			self.alogfile = open('host_' + self.agent_IP + "_" + str(childs_pid) + '.log', "a", 1)
			self.amessage("in child" + str(childs_pid))
			self.check_instance()
		else:
			self.agent_pids.append(pid)
			self.IPs_pids[self.agent_IP] = pid
		return pid

	def get_agentIP_from_pid(self, pid):
		return self.IPs_pids.keys()[self.IPs_pids.values().index(pid)]

	def check_instance(self):
		self.ssh_agent_version_response = self.get_ssh_version()
		self.ssh_agent_update_response = self.update_ssh_agent()
		self.ssh_agent_no_pswd_response = self.check_set_no_pswd()
#		self.cleanup()

	def get_ssh_version(self):
		cmd = self.ssh_version_cmd + ' ' + self.agent_IP + ' 22; exit 0'
		output = subprocess.check_output(cmd, stderr= subprocess.STDOUT, shell=True)
		self.amessage("openSSH Version:  " + output.split("\n")[0])
		return

	def update_ssh_agent(self):
		cmd = self.ssh_agent_cmd +' ' + self.ssh_user + '@' + self.agent_IP
		cmd += ' cat /etc/os-release'
		output = self.run_command(cmd)
		if 'centos' in output:
			self.release == 'centos'
			cmd = self.ssh_agent_cmd + ' ' + self.ssh_user + '@' + self.agent_IP + ' ' + self.sshd_update_centos
			output = self.run_command(cmd)
			self.amessage("yum update result for " + self.agent_IP + ": " + output)
		elif 'ubuntu' in output:
			cmd = self.ssh_agent_cmd + ' ' + self.ssh_user + '@' +self.agent_IP + ' ' +self.sshd_update_ubuntu
			output = self.run_command(cmd)
			self.amessage("apt update result for " + self.agent_IP + ": " + output)
		else:
			self.amessage("error: host " + self.agent_IP + " not ubuntu nor centos type.\njerror can't update OpenSSH server")

	def check_set_no_pswd(self):
		cmd = self.ssh_agent_cmd + ' ' + self.ssh_user + '@' + self.agent_IP
		cmd += ' sudo cat /etc/ssh/sshd_config'
		output = self.run_command(cmd)
		self.amessage("sudo cat /etc/ssh/sshd_config result for " + self.agent_IP + ": " + output)
		output = self.run_command(cmd)

		if not bool(re.search(r'\nPasswordAuthentication no\n', output)):
			cmd = self.ssh_agent_cmd + ' ' + self.ssh_user + '@' + self.agent_IP
			cmd +=' sudo sed -i -e \'\$aPasswordAuthentication\ no\' /etc/ssh/sshd_config'
			output = self.run_command(cmd)
			self.amessage('Added missing "PasswordAuthentication no" to file on ' + self.agent_IP)
		else:
			self.amessage('"PasswordAuthentication no" already exists on ' + self.agent_IP)

	def get_date(self):
		return time.strftime("%x").replace("/", "")

	def run_command(self, cmd):
		try:
			output = subprocess.check_output(cmd, stderr= subprocess.STDOUT, shell=True)
		except Exception as e:
			self.amessage("error " + e.output)
			return "error " + e.output
		return output

	def mmessage(self, msg):
		self.mlogfile.write( msg + "\n")

	def amessage(self, msg):
		self.alogfile.write( msg + "\n")

def setup_argparser():
	parser = argparse.ArgumentParser()
	parser = argparse.ArgumentParser(description='logs into systems IPs in hosts.txt file and checks on SSHD version, sshd config settings to centos & ubuntu type systems')
	parser.add_argument('-pri', help='private key file name. Defaults to \"developer\" if omitted', required=False)
	parser.add_argument('-pri_dir', help='private key file location. Defaults to \"~/.ssh/developer_key\" if omitted', required=False)
	parser.add_argument('-ssh_user', help='ssh login user. Defaults to \"developeer\" if omitted', required=False)
	parser.add_argument('-rm_logs', help='removes all log files before starting', required=False, action="store_true")
	parser.add_argument('-hosts_file', help='hosts file to check.  Uses \"./hosts.txt\" if omitted', required=False, default='./hosts.txt')
	return parser.parse_args()


def get_hosts(hosts_file):
	return open(hosts_file, "rb").readlines()


def job():
	args = setup_argparser()
	if args.rm_logs:
		subprocess.check_output('/bin/rm -rf *log exit 0', shell=True)
	tool = SSHTool(args)
	print('working'),
	for agent_IP in get_hosts(args.hosts_file):
		tool.set_agent_IP(agent_IP.strip())
		if tool.fork_agent() != 0:
			exit(0)
		time.sleep(.100)  # wait .1 secs between invocations
		tool.mmessage("Agent started on instance:" + agent_IP)
	num = len(tool.get_IPs())
	print("waiting " + str(num/10 + 15) + " seconds for " + str(num) + " instance checks")
	for i in range(0, num * 10 + 150, 10):
		time.sleep(1)
		print '.',
		sys.stdout.flush()
	else:
		print

	terminations = False
	for agent_pid in tool.get_all_pids():
		if os.path.isdir('/proc/{}'.format(pid)):
			os.kill(pid, 9)  # kill if still running
			tool.mmessage("host " + get_agentIP_from_pid(sgent_pid) + " ssh needed to be terminated")
			terminations = True
		tool.mmessage("The following exceptions have occurred: ")
	exception_data = ''
	all_log_files = subprocess.check_output('/bin/ls host*log; exit 0', shell=True).split()
	for log in all_log_files:
		output = subprocess.check_output("/bin/grep error " + log.strip() + ";exit 0", shell=True)
		if len(output) > 0:
			exception_data += "see individual log file: " + log.strip() + "\n"
	if exception_data != '':
		tool.mmessage("The following exceptions have occurred: ")
		tool.mmessage(exception_data)
		print("exceptions have occurred. See master.log for summary and agent logfile(s) for details")
	else:
		print("No exceptions have occurred. See master.log for summary and Agent log(s) for details")
	if terminations:
		print("terminations have occurred. See master.log for summary and Agent log(s) for details")
	else:
		print("No terminations have occurred. See master.log for summary and Agent log(s) for details")


if __name__ == "__main__":
	job()


